<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questionari extends Model
{
    public function user(){
        //N
        return $this->belongsTo(User::class);
    }

    public function pregunta(){
        return $this->hasMany(Pregunta::class);
    }
}
