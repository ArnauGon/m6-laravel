<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Intent extends Model
{
    use HasFactory;

    public function questionari(){
        return $this->belongsTo(Questionari::class);
    }

    public function resposta(){
        return $this->hasMany(Resposta::class);
    }
}
