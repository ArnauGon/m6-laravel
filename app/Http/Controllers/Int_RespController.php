<?php

namespace App\Http\Controllers;

use App\Models\Pregunta;
use App\Models\Questionari;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;


class Int_RespController extends Controller
{
    public function create(Request $request){
        if (Auth::check()){

            $int_resp = new Int_Resp();

            $int_resp->descripcio = $request->descripcio;

            $int_resp->save();
            return redirect('/home');

        }
    }

    public function int_resp(){
        return view('int_resp');
    }


    public function getByUser(User $user){
        return view ('ranking', compact('user'));
    }

    public function ranking(){
        return view('ranking');
    }
}
