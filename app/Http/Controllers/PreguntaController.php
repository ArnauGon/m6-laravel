<?php

namespace App\Http\Controllers;

use App\Models\Pregunta;
use App\Models\Questionari;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;


class PreguntaController extends Controller
{
    public function create(Request $request,Questionari $questionari){
        if (Auth::check()){
            $User = Auth::user();
            if($User->kiboy) {
                $pregunta = new Pregunta();

                $pregunta->enunciado = $request->enunciado;
                $pregunta->respuesta = $request->respuesta;
                $pregunta->questionari_id = $questionari->id;

                $pregunta->save();
                return redirect('/home');
            }

            return redirect('/notkiboy');
        }
    }

    public function pregunta(){
        return view('pregunta');
    }


    public function selecionarQuestionari(Questionari $questionari){
        return view ('pregunta', compact('questionari'));
    }

    public function ranking(){
        return view('ranking');
    }
}
