<?php

namespace App\Http\Controllers;

use App\Models\Pregunta;
use App\Models\Questionari;
use App\Models\Resposta;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;


class RespostaController extends Controller
{
    public function create(Request $request){
        if (Auth::check()){

            $resposta = new Resposta();

            $resposta->texte = $request->texte;
            $resposta->nota = $request->nota;

            $resposta->save();
            return redirect('/home');

        }
    }

    public function resposta(){
        return view('resposta');
    }


    public function getByUser(User $user){
        return view ('ranking', compact('user'));
    }

    public function ranking(){
        return view('ranking');
    }
}
