<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome',function (){
    return 'Hola Mon!!';
});

Route::get('/notkiboy', function () {
    return view('notkiboy');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/ranking', [App\Http\Controllers\QuestionariController::class, 'ranking'])->name('ranking');
Route::get('/ranking/getByUser/{user}', 'App\Http\Controllers\QuestionariController@ranking');

Route::get('/questionari','App\Http\Controllers\QuestionariController@questionari');
Route::post('/questionari','App\Http\Controllers\QuestionariController@create');

Route::get('/pregunta/{questionari}','App\Http\Controllers\PreguntaController@selecionarQuestionari');
Route::post('/pregunta/{questionari}','App\Http\Controllers\PreguntaController@create');

Route::get('/resposta','App\Http\Controllers\Int_RespController@int_resp');
Route::post('/resposta','App\Http\Controllers\Int_RespController@create');


