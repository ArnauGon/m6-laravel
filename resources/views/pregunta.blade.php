@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Formula la teva pregunta</h2>

        <form method="POST" action="/pregunta/{{$questionari->id}}">

            <div class="form-group">
                <label>Enunciado </label><input name="enunciado" class="form-control"></input>
            </div>
            <div class="form-group">
                <label> Respuesta </label><textarea name="respuesta" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add Task</button>
            </div>
            {{ csrf_field() }}
        </form>



    </div>
@endsection
