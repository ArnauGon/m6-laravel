@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Crea la teva resposta</h2>

        <form method="POST" action="/resposta">

            <div class="form-group">
                <label>resposta </label><input name="texte" class="form-control"></input>
            </div>
            <div class="form-group">
                <label>Nota </label><textarea name="nota" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add Task</button>
            </div>
            {{ csrf_field() }}
        </form>

    </div>
@endsection
