@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Formula el teu formulari</h2>

        <form method="POST" action="/questionari">

            <div class="form-group">
                <label>Nom del questionari </label><input name="nombre" class="form-control"></input>
            </div>
            <div class="form-group">
                <label>Nota </label><textarea name="nota" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add Task</button>
            </div>
            {{ csrf_field() }}
        </form>



    </div>
@endsection

